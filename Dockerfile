FROM debian:12-slim

MAINTAINER Jan Gaus <jan.gaus@cyqon.de>

RUN apt update --allow-releaseinfo-change && \
    apt -y install ca-certificates \
                   apt-transport-https \
                   lsb-release \
                   wget \
                   gnupg2 \
                   openssl

RUN echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/sury-php.list
RUN wget -qO - https://packages.sury.org/php/apt.gpg | apt-key add -
#RUN wget -qO - https://packages.sury.org/php/apt.gpg | gpg --dearmor -o /usr/share/keyrings/php-archive-keyring.gpg \
#    echo "deb [signed-by=/usr/share/keyrings/php-archive-keyring.gpg] https://packages.sury.org/php/ $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/php.list > /dev/null
RUN apt update

RUN apt update --allow-releaseinfo-change && \
    apt -y install curl \
                   unzip \
                   nginx \
                   supervisor \
                   net-tools \
                   nano cron \
                   php8.2-fpm \
                   php8.2-gd \
                   php8.2-intl \
                   php8.2-cli \
                   php8.2-curl \
                   php8.2-mysql \
                   php8.2-opcache \
                   php8.2-mbstring \
                   php8.2-ldap \
                   php8.2-zip \
                   php8.2-xml \
                   php8.2-soap \
                   php8.2-pdo-mysql \
                   php8.2-pgsql \
                   php8.2-imagick && \
    sed -i -e "s/worker_processes  1/worker_processes 8/" /etc/nginx/nginx.conf && \
    echo "daemon off;" >> /etc/nginx/nginx.conf &&           \
    sed -i 's/memory_limit = [^ ]*/memory_limit = 512M/' /etc/php/8.2/fpm/php.ini && \
    sed -i 's/upload_max_filesize = [^ ]*/upload_max_filesize = 200M/' /etc/php/8.2/fpm/php.ini && \
    sed -i 's/max_execution_time = [^ ]*/max_execution_time = 600/' /etc/php/8.2/fpm/php.ini && \
    sed -i 's/memory_limit = [^ ]*/memory_limit = -1/' /etc/php/8.2/cli/php.ini && \
    sed -i 's/post_max_size = [^ ]*/post_max_size = 200M/' /etc/php/8.2/fpm/php.ini && \
    sed -i 's/pm = [^ ]*/pm = static/' /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i 's/pm.max_children = [^ ]*/pm.max_children = 8/' /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i 's/;catch_workers_output = [^ ]*/catch_workers_output = yes/' /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i 's/;clear_env = [^ ]*/clear_env = no/' /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i 's/;daemonize = [^ ]*/daemonize = no/' /etc/php/8.2/fpm/php-fpm.conf  && \
    mkdir -p /run/php/ && \
    touch /run/php/php8.2-fpm.sock

ADD ./php-fpm/00-ioncube.ini /etc/php/8.2/fpm/conf.d/00-ioncube.ini
ADD ./php-fpm/00-ioncube.ini /etc/php/8.2/cli/conf.d/00-ioncube.ini
ADD ./php-fpm/ioncube_loader_lin_8.2.so /usr/lib/php/20220829/
ADD ./php-fpm/ioncube_loader_lin_8.2_ts.so /usr/lib/php/20220829/

ADD ./nginx/nginx.conf /etc/nginx/sites-available/default
ADD ./nginx/php_params /etc/nginx/php_params
ADD ./supervisor/supervisord.conf /etc/supervisord.conf

WORKDIR /srv

# CRONTAB
ADD crontab /etc/cron.d/laravel
RUN chmod 0644 /etc/cron.d/laravel

# COMPOSER
RUN curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer && \
    chmod a+x /usr/local/bin/composer

# GULP
RUN apt-get install -y gnupg && \
    curl -sL https://deb.nodesource.com/setup_21.x | bash - && \
    apt-get install -y nodejs && \
    npm install --global gulp-cli

RUN npm install --global yarn

ENV TZ=Europe/Berlin

EXPOSE 80

CMD env > /etc/environment; supervisord -c /etc/supervisord.conf